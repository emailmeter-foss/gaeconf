=======
Gaeconf
=======


.. image:: https://img.shields.io/pypi/v/gaeconf.svg
        :target: https://pypi.python.org/pypi/gaeconf

.. image:: https://readthedocs.org/projects/gaeconf/badge/?version=latest
        :target: https://gaeconf.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




CLI utility to generate .yaml GAE configurations


* Free software: MIT license
* Documentation: https://gaeconf.readthedocs.io.


Features
--------

* CLI interface that receives a yaml config module and a GAE service name and outputs a valid GAE app-yaml file

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
