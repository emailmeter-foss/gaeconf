=======
History
=======

0.1.0 (2019-06-26)
------------------

* First release on PyPI.

0.1.1 (2019-06-27)
------------------

* Minor changes in setup.py

0.1.2 (2019-06-27)
------------------

* Adds `-e` option in CLI to expose host environment variables
